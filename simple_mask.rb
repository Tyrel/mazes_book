require 'recursive_backtrack'
require 'mask'
require 'masked_grid'

mask = Mask.new(5,5)

mask[0,0] = false
mask[2,2] = false
mask[4,4] = false

grid = MaskedGrid.new(mask)
RecursiveBacktrack.on(grid)

puts grid