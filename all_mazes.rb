require 'colored_grid'
require 'aldous_broder'
require 'binary_tree'
require 'distance_grid'
require 'sidewinder'
require 'wilsons'
require 'hunt_and_kill'
require 'recursive_backtrack'
require 'mask'
require 'masked_grid'

def gen_maze grid, name
  start = grid.random_cell
  distances = start.distances
  new_start, distance = distances.max
  puts "First Distance: #{distance} - #{name}"
  new_distances = new_start.distances
  goal, distance = new_distances.max
  puts "End Distance: #{distance} - #{name}"
  puts "Dead Ends: #{grid.deadends.count} - #{name}"
  filename = "img_#{name}.png"
  grid.distances = new_distances.path_to(goal)
  grid.to_png.save(filename)
  puts
end

H = 5
W = 5

#mazes = [Wilsons, AldousBroder, HuntAndKill, RecursiveBacktrack]
mazes = [RecursiveBacktrack]


mazes.each do |maze|
  grid = ColoredGrid.new(H, W)
  grid[0,0].east.west = nil
  grid[0,0].south.north = nil

  grid[4,4].west.east = nil
  grid[4,4].north.south = nil

  maze.on(grid, start_at: grid[1,1])
  gen_maze(grid, maze.name)
end
