require 'grid'
require 'chunky_png'

class ColoredGrid < Grid
  def distances=(distances)
    @distances = distances
    farthest, @maximum = distances.max
  end

  def background_color_for_cell(cell)
    distance = @distances[cell] or return nil
    intensity = (@maximum - distance).to_f / @maximum
    dark = (255 * intensity).round
    bright = 128+(2*intensity).round
    ChunkyPNG::Color.rgb(dark,bright,bright)
  end
end