require 'distance_grid'
require 'sidewinder'
H = 11
W = 22

grid = DistanceGrid.new(H, W)
Sidewinder.on(grid)

start = grid[0,0]
distances = start.distances

grid.distances = distances
puts grid
puts ""
puts ""
puts "path from nw to sw"
grid.distances = distances.path_to(grid[grid.rows-1, grid.columns-1])
puts grid.to_s